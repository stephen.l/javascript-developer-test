const { data } = require("../data");
const { filter, count } = require("./functions");
const { run } = require("./run");

jest.mock('./functions', () => {
    return {
        filter: jest.fn(() => undefined),
        count: jest.fn(() => undefined)
    };
  });


describe('run', () => {
    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('Should throw an error when there is no argument', () => {
        expect(run).toThrow(new Error('An argument is expected'))
    });

    it('Should throw an error when the argument is not valid', () => {
        jest.replaceProperty(process, 'argv', ['what', 'ever', 'you', 'like']);
        expect(run).toThrow(new Error('Invalid Argument'))

    });

    it('Should call the filter function', () => {
        jest.replaceProperty(process, 'argv', ['/usr/local/bin/node', '/Users/mjr/work/node/process-args.js', '--filter=ry']);
        run();
        expect(filter).toHaveBeenCalledWith(data, 'ry');
    });

    it('Should call the count function', () => {
        jest.replaceProperty(process, 'argv', ['/usr/local/bin/node', '/Users/mjr/work/node/process-args.js', '--count']);
        run();
        expect(count).toHaveBeenCalledWith(data);
    });
});