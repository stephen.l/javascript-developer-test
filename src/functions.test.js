const { count, filter } = require("./functions");

describe('Functions', () => {
    describe('filter', () => {
        it('Should filter on the given pattern', () => {
            const mock = [{
                name: 'Dillauti',
                people: [{
                    name: 'Winifred Graham',
                    animals: [
                        {name: 'Anoa'},
                        {name: 'Crow'}]
                    },
                    {
                        name: 'Bobby Ristori',
                        animals: [
                            {name: 'Kowari'},
                            {name: 'Caecilian'},
                            {name: 'Linne\'s Two-toed Sloth'}
                        ]
                    }]
                },
                {
                  name: 'Tohabdal',
                  people:
                    [
                      {
                        name: 'Essie Bennett',
                        animals:
                          [{name: 'Aldabra Tortoise'},
                            {name: 'Aldabra Tortoise'}]
                      },
                      {
                        name: 'Owen Bongini',
                        animals:
                          [{name: 'Zebrashark'},
                            {name: 'African Wild Dog'}]
                      },
                      {
                        name: 'Randall Benoît',
                        animals:
                          [{name: 'Chameleons'},
                            {name: 'King Vulture'},
                            {name: 'Sand Cat'}]
                      }]
                }
            ];

            const expectedResult = [{
                name: 'Dillauti',
                people: [{
                    name: 'Winifred Graham',
                    animals: [
                        {name: 'Anoa'},
                        {name: 'Crow'}
                    ]
                },
                {
                    name: 'Bobby Ristori',
                    animals: [
                        {name: 'Kowari'},
                        {name: 'Linne\'s Two-toed Sloth'}
                    ]
                }]},
                {
                  name: 'Tohabdal',
                  people:
                    [
                      {
                        name: 'Essie Bennett',
                        animals:
                          [{name: 'Aldabra Tortoise'},
                            {name: 'Aldabra Tortoise'}]
                      },
                      {
                        name: 'Owen Bongini',
                        animals:
                          [{name: 'African Wild Dog'}]
                      },
                      {
                        name: 'Randall Benoît',
                        animals:
                          [{name: 'Chameleons'}]
                      }]
                }
            ];

            expect(filter(mock, 'o')).toEqual(expectedResult);
        });

        it('Should remove persons with no animals matching the pattern', () => {
            const mock = [{
                name: 'Dillauti',
                people: [{
                    name: 'Winifred Graham',
                    animals: [
                        {name: 'Anoa'},
                        {name: 'Crow'}]
                    },
                    {
                        name: 'Bobby Ristori',
                        animals: [
                            {name: 'Kowari'},
                            {name: 'Caecilian'},
                            {name: 'Linne\'s Two-toed Sloth'}
                        ]
                    }]
                },
                {
                  name: 'Tohabdal',
                  people:
                    [
                      {
                        name: 'Essie Bennett',
                        animals:
                          [{name: 'Aldabra'}]
                      },
                      {
                        name: 'Owen Bongini',
                        animals:
                          [{name: 'Zebrashark'},
                            {name: 'African Wild Dog'}]
                      },
                      {
                        name: 'Randall Benoît',
                        animals:
                          [{name: 'Chameleons'},
                            {name: 'King Vulture'},
                            {name: 'Sand Cat'}]
                      }]
                }
            ];

            const expectedResult = [{
                name: 'Dillauti',
                people: [{
                    name: 'Winifred Graham',
                    animals: [
                        {name: 'Anoa'},
                        {name: 'Crow'}
                    ]
                },
                {
                    name: 'Bobby Ristori',
                    animals: [
                        {name: 'Kowari'},
                        {name: 'Linne\'s Two-toed Sloth'}
                    ]
                }]},
                {
                  name: 'Tohabdal',
                  people:
                    [
                      {
                        name: 'Owen Bongini',
                        animals:
                          [{name: 'African Wild Dog'}]
                      },
                      {
                        name: 'Randall Benoît',
                        animals:
                          [{name: 'Chameleons'}]
                      }]
                }
            ];

            expect(filter(mock, 'o')).toEqual(expectedResult);
        });

        it('Should remove countries with no persons matching the pattern', () => {
            const mock = [{
                name: 'Dillauti',
                people: [{
                    name: 'Winifred Graham',
                    animals: [
                        {name: 'Anoa'},
                        {name: 'Crow'}]
                    },
                    {
                        name: 'Bobby Ristori',
                        animals: [
                            {name: 'Kowari'},
                            {name: 'Caecilian'},
                            {name: 'Linne\'s Two-toed Sloth'}
                        ]
                    }]
                },
                {
                  name: 'Tohabdal',
                  people:
                    [
                      {
                        name: 'Essie Bennett',
                        animals:
                          [{name: 'Aldabra'}]
                      },
                      {
                        name: 'Owen Bongini',
                        animals:
                          [{name: 'Zebrashark'}]
                      },
                      {
                        name: 'Randall Benoît',
                        animals:
                          [
                            {name: 'King Vulture'},
                            {name: 'Sand Cat'}]
                      }]
                }
            ];

            const expectedResult = [{
                name: 'Dillauti',
                people: [{
                    name: 'Winifred Graham',
                    animals: [
                        {name: 'Anoa'},
                        {name: 'Crow'}
                    ]
                },
                {
                    name: 'Bobby Ristori',
                    animals: [
                        {name: 'Kowari'},
                        {name: 'Linne\'s Two-toed Sloth'}
                    ]
                }]}
            ];

            expect(filter(mock, 'o')).toEqual(expectedResult);
        });

        it('Should return undefined if no countries is matching', () => {
            const mock = [{
                name: 'Dillauti',
                people: [{
                    name: 'Winifred Graham',
                    animals: [
                        {name: 'Anoa'},
                        {name: 'Crow'}]
                    },
                    {
                        name: 'Bobby Ristori',
                        animals: [
                            {name: 'Kowari'},
                            {name: 'Caecilian'},
                            {name: 'Linne\'s Two-toed Sloth'}
                        ]
                    }]
                },
                {
                  name: 'Tohabdal',
                  people:
                    [
                      {
                        name: 'Essie Bennett',
                        animals:
                          [{name: 'Aldabra Tortoise'},
                            {name: 'Aldabra Tortoise'}]
                      },
                      {
                        name: 'Owen Bongini',
                        animals:
                          [{name: 'Zebrashark'},
                            {name: 'African Wild Dog'}]
                      },
                      {
                        name: 'Randall Benoît',
                        animals:
                          [{name: 'Chameleons'},
                            {name: 'King Vulture'},
                            {name: 'Sand Cat'}]
                      }]
                }
            ];

            expect(filter(mock, 'abcd')).toEqual(undefined);
        });
    });

    describe('count', () => {
        it('Should return the data with the count on each name', () => {
            const mock = [{
                name: 'Dillauti',
                people: [{
                    name: 'Winifred Graham',
                    animals: [
                        {name: 'Anoa'},
                        {name: 'Crow'}]
                    },
                    {
                        name: 'Bobby Ristori',
                        animals: [
                            {name: 'Kowari'},
                            {name: 'Caecilian'},
                            {name: 'Linne\'s Two-toed Sloth'}
                        ]
                    }]
                },
                {
                  name: 'Tohabdal',
                  people:
                    [
                      {
                        name: 'Essie Bennett',
                        animals:
                          [{name: 'Aldabra Tortoise'},
                            {name: 'Aldabra Tortoise'}]
                      },
                      {
                        name: 'Owen Bongini',
                        animals:
                          [{name: 'Zebrashark'},
                            {name: 'African Wild Dog'}]
                      },
                      {
                        name: 'Randall Benoît',
                        animals:
                          [{name: 'Chameleons'},
                            {name: 'King Vulture'},
                            {name: 'Sand Cat'}]
                      }]
                }
            ];

            const expectedResult = [{
                name: 'Dillauti [2]',
                people: [{
                    name: 'Winifred Graham [2]',
                    animals: [
                        {name: 'Anoa'},
                        {name: 'Crow'}
                    ]
                },
                {
                    name: 'Bobby Ristori [3]',
                    animals: [
                        {name: 'Kowari'},
                        {name: 'Caecilian'},
                        {name: 'Linne\'s Two-toed Sloth'}
                    ]
                }]},
                {
                  name: 'Tohabdal [3]',
                  people:
                    [
                      {
                        name: 'Essie Bennett [2]',
                        animals:
                          [{name: 'Aldabra Tortoise'},
                            {name: 'Aldabra Tortoise'}]
                      },
                      {
                        name: 'Owen Bongini [2]',
                        animals:
                          [{name: 'Zebrashark'},
                            {name: 'African Wild Dog'}]
                      },
                      {
                        name: 'Randall Benoît [3]',
                        animals:
                          [{name: 'Chameleons'},
                            {name: 'King Vulture'},
                            {name: 'Sand Cat'}]
                      }]
                }
            ];

            expect(count(mock)).toEqual(expectedResult);
        });
    });
});