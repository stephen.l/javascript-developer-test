function filter(data, pattern) {
    const filteredCountries = data.reduce(((countries,country) => {
        const filteredPeople = country.people.reduce(((people, person) => {
            const filteredAnimals = person.animals.filter(animal => animal.name.includes(pattern));
            
            if (filteredAnimals.length > 0) {
                people.push({
                    name: person.name,
                    animals: filteredAnimals
                });
            }

            return people;
        }), []);

        if (filteredPeople.length > 0) {
            countries.push({
                name: country.name,
                people: filteredPeople
            });
        }

        return countries;
    }), []);

    return filteredCountries.length > 0 ? filteredCountries : undefined;
}

function count(data) {
    const dataWithCount = data.map(country => {
        return {
            name: `${country.name} [${country.people.length}]`,
            people: country.people.map(person => {
                return {
                    name: `${person.name} [${person.animals.length}]`,
                    animals: person.animals
                }
            })
        }
    })
    
    return dataWithCount;
}

module.exports = {
    filter,
    count
}