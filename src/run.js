const { data } = require('../data');
const { filter, count } = require('./functions');

function run(){
    const arg = process.argv[2];

    if(!arg){
        throw new Error('An argument is expected');
    }

    const command = arg.split('=');
    let result;

    switch (command[0]) {
        case '--filter':
            result = filter(data, command[1]);
            break;
        case '--count':
            result = count(data);
            break;
        default:
            throw new Error('Invalid Argument')
    }

    console.log(JSON.stringify(result, null, 2));

    return;
}

module.exports = {
    run
}