const { run } = require('./src/run');

try {
    run();
} catch (error) {
    console.error('Error:', error.message)
}